#!/dev/null

_distribution_archlinux_dependencies=(
	# mOSAIC custom packages
	# Archlinux tools packages
		zip
	# Archlinux language packages
		jdk7-openjdk
		python2
		perl
		gcc
	# Archlinux development packages
		git
		binutils
		libtool
		autoconf
		automake
		pkg-config
		make
	# Archlinux library packages
		jansson
		libxml2
)
