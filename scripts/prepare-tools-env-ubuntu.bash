#!/dev/null

_distribution_ubuntu_dependencies=(
	# mOSAIC custom packages
		mosaic-sun-jdk-7u1 #### missing
	# Ubuntu tools packages
		tar
		zip
		wget
		curl
	# Ubuntu language packages
		python
		perl
		gcc
		g++
	# Ubuntu development packages
		git
		binutils
		libtool
		autoconf
		automake
		pkg-config
		make
	# Ubuntu library packages
		jansson #### missing
		libxml2-dev
		uuid-dev
)
